<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 11/20/18
 * Time: 9:14 AM
 */

namespace catawich\models;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model {

    use SoftDeletes;

    protected $table = 'categorie';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
    public $timestamps = false;


    public function sandwichs(){
        return $this->belongsToMany('catawich\models\Sandwich',
            'sand2cat',
            'cat_id',
            'sand_id');
    }


}