<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 11/20/18
 * Time: 9:13 AM
 */
namespace catawich\models;
use Illuminate\Database\Eloquent\Model;

class Taille extends Model{

    use SoftDeletes;

    protected $table = 'taille_sandwich';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    public function sandwiches(){
        return $this->belongsToMany('catawich\models\Sandwich', 'tarif', 'taille_id', 'sand_id')
            ->withPivot(['prix']);
    }
}