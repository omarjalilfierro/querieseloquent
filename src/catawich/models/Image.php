<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 11/20/18
 * Time: 9:14 AM
 */
namespace catawich\models;
use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    use SoftDeletes;

    protected $table = 'image';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    public function sandwich(){
        return self::belongsTo('catawich\models\Sandwich','s_id');
    }
}