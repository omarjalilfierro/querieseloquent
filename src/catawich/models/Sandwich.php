<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 11/20/18
 * Time: 9:14 AM
 */
namespace catawich\models;
use Illuminate\Database\Eloquent\Model;

class Sandwich extends Model {

    use SoftDeletes;

    protected $table = 'sandwich';
    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    public function categories(){
        return $this->belongsToMany('catawich\models\Categorie',
            'sand2cat',
            'sand_id',
            'cat_id');
    }

    public function images(){
        return self::hasMany('catawich\models\Image','s_id');
    }

    public function tailles(){
        return $this->belongsToMany('catawich\models\Taille', 'tarif', 'sand_id', 'taille_id')->withPivot(['prix']);
    }

}