<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 11/27/18
 * Time: 10:30 AM
 */
require_once 'vendor/autoload.php';

use catawich\models\Categorie;
use catawich\models\Image;
use catawich\models\Sandwich;
use catawich\models\Taille;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Functions
{
    /*Simple Requests*/
//1.1. lister les sandwichs du catalogue, afficher leur nom, description, type de pain,
    public function e11()
    {
        echo Sandwich::query()->select(['nom', 'description', 'type_pain'])->get();
    }

//1.2. idem, en triant selon le type_pain,
    public function e12()
    {
        echo Sandwich::query()->select(['nom', 'description', 'type_pain'])->orderBy('type_pain')->get();
    }

//1.3. afficher le sandwich n° 42 s'il existe, sinon afficher un message indiquant qu'il n'existe pas.
//Utiliser l'exception ModelNotFoundException.
    public function e13()
    {
        try {
            $sandwich42 = Sandwich::where('id', '=', 42)->firstOrFail();
        } catch (ModelNotFoundException $error) {
            echo "Sandwich existe pas " . $error->getMessage();
        }
    }


//1.4. afficher les sandwichs dont le type_pain contient 'baguette', triés par type_pain
    public function e14()
    {
        echo Sandwich::where('type_pain', 'baguette')->orderBy('type_pain')->get();
    }

//1.5. créer un nouveau sandwich et l'insérer dans la base.
    public function e15()
    {
        $sandwich = new Sandwich();
        $sandwich->nom = "Jalilo";
        $sandwich->description = "Tres bon";
        $sandwich->type_pain = "Tortilla";
        $sandwich->save();
    }


    /************************************************Associations 1-n*******************************************************/
//2.1. afficher le sandwich n°4 et lister les images associées,
    public function e21()
    {
        echo Sandwich::query()->select(['sandwich.*', 'image.*'])
            ->join('image', 'sandwich.id', '=', 'image.s_id')
            ->where('sandwich.id', 4)->get();
    }


//2.2. lister l'ensemble des sandwichs, triés par type de pain, et pour chaque sandwich afficher la
//liste des images associées. Utiliser un chargement lié.
    public function e22()
    {
        $sandwich = Sandwich::with('images')->orderBy('type_pain')->get();
        foreach ($sandwich as $sand)
            echo 'nom: ' . $sand->nom . '  ' . $sand->images->first()->titre;
    }

//2.3. lister les images et indiquer pour chacune d'elle le sandwich associé en affichant son nom et
//son type de pain.
    public function e23()
    {
        $sandwich = Sandwich::with('images')->get();
        foreach ($sandwich as $sand)
            echo ' IMAGE: ' . $sand->images->first()->titre
                . ' NOM SANDWICH: ' . $sand->nom
                . ' TYPE PAIN: ' . $sand->type_pain . "\n";
    }

//2.4. créer 3 images associées au sandwich ajouté dans l'exercice 1.
    public function e24()
    {
        $sand = Sandwich::find(11); //Sandwich 1.5

        for ($i = 0; $i < 3; $i++) {
            $image = new Image();
            $image->titre = "image1";
            $image->type = "image/png";
            $image->def_x = "855";
            $image->def_y = "232";
            $image->taille = "4952";
            $image->filename = "image648.png";

            $image->sandwich()->associate($sand)->save();
        }
    }


//2.5. changer le sandwich associé à la 3ème image créée et le remplacer par le sandwich d'Id 6
    public function e25()
    {
        $image = Image::find(3);
        $sandwich = Sandwich::find(6);
        $image->sandwich()->assosiate($sandwich)->save();
    }

    /************************************************Associations N-N **********************************************************/
//3.1. lister les catégories du sandwich d'ID 5 ; afficher le sandwich (nom, description, type de
//pain) et le nom de chacune des catégories auxquelles il est associé.
    public function e31()
    {
        $sand = Sandwich::find(5);
        echo "Nom: $sand->nom \n";
        echo "Description: $sand->description \n";
        echo "Type de Pain: $sand->type_pain \n";
        foreach ($sand as $s) {
            $cat = $sand->categories->first()->nom;
            echo "Nom de categorie: $cat \n";
        }
    }

//3.2. lister l'ensemble des catégories, et pour chaque catégorie la liste de sandwichs associés ;
//utiliser un chargement lié.
    public function e32()
    {
        $categories = Categorie::with('sandwichs')->get();
        foreach ($categories as $cat) {
            echo "Categorie nom: $cat->nom \n";
            foreach ($cat->sandwichs as $sand) {
                echo "Nom Sandwich $sand->nom \n";
            }
        }
    }

//3.3. lister les sandwichs dont le type_pain contient 'baguette' et pour chaque sandwich, afficher
//ses catégories et la liste des images qui lui sont associées ; utiliser un chargement lié.
    public function e33()
    {
        $sandich = Sandwich::with('categories')->with('images')->where('type_pain', 'like', '%baguette%')->get();
        foreach ($sandich as $sand) {
            echo "NOM: $sand->nom \n";
            echo "Categories: ";
            foreach ($sand->categories as $category) {
                echo $category->nom . ", ";
            }
            echo "\n Images ";
            foreach ($sand->images as $image) {
                echo $image->titre . ", ";
            }
        }

    }

//3.4. associer le sandwich créé au 1.5 aux catégories 1 et 3.
    public function e34()
    {
        $sandwich = Sandwich::find(11); //sandwich 1.5
        $sandwich->categories()->attach([1, 3]);

    }

    /************************************************ Attributs d'associations**********************************************************/

//4.1. afficher la liste des tailles proposées pour le sandwich d'ID 5
    public function e41()
    {
        $sandwich = Sandwich::with('tailles')->where('id', 5)->get()->first();
        foreach ($sandwich->tailles as $taille) {
            echo "Tailles: $taille";
        }
    }

//4.2. idem, mais en ajoutant le prix du sandwich pour chaque taille
    public function e42()
    {
        $sandwich = Sandwich::with('tailles')->where('id', 5)->get()->first();
        foreach ($sandwich->tailles as $taille) {
            echo "Tailles: $taille \n";
            echo "Prix: " . $taille->pivot->prix . "\n";
        }
    }

//4.3. associer le sandwich créé au 1.5 aux différentes tailles existantes en précisant le prix dans
//chaque cas.
    public function e43()
    {
        $sandwich = Sandwich::find(11); //sandwich 1.5
        $sandwich->tailles()->attach([1 => ['prix' => 5.00], 2 => ['prix' => 7.00]]);

    }

    /********************************************* Requêtes sur des associations **********************************************************/

//5.1. pour la catégorie dont le nom contient 'traditionnel', lister les sandwichs dont le type_pain
//contient 'baguette',
    public function e51()
    {
        $categorie = Categorie::with('sandwichs')->where('nom', 'like', '%traditionnel%')->get()->first();
        $sandwiches = $categorie->sandwichs()->where('type_pain', 'like', 'baguette')->get()->first();
        echo $sandwiches . "\n";
    }

//5.2. pour le sandwich d'ID 5, lister les images de type 'image/jpeg' et de def_X > 720,
    public function e52()
    {
        $images = Image::where('type', 'image/jpeg')->where('def_X', '>', 720)->where('s_id', 5)->get();
        foreach ($images as $image) {
            echo $image . "\n";
        }
    }

//5.3. lister les sandwichs qui ont plus de 4 images associées,
    public function e53()
    {
        $sandwiches = Sandwich::all();
        foreach ($sandwiches as $sandwich) {
            $count = $sandwich->images()->count();
            if ($count > 4) {
                echo "Sandwich: $sandwich \n";
            }
        }

    }

//5.4. lister les catégories qui ont plus de 6 images associées,
    public function e54()
    {
        $sandwiches = Sandwich::all();
        foreach ($sandwiches as $sandwich) {
            $count = $sandwich->images()->count();
            if ($count > 6) {
                echo "Sandwich: $sandwich \n";
            }
        }

    }

//5.5. lister les catégories qui contiennent des sandwichs dont le type de pain est 'baguette',
    public function e55()
    {
        $sandwiches = Sandwich::with('categories')->where('type_pain', 'baguette')->first();
        foreach ($sandwiches->categories as $category) {
            echo "Category: $category \n";
        }

    }

//5.6. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000
    public function e56()
    {
        $images = Image::with('sandwich')->where('type', 'image/jpeg')
            ->where('taille', '>', '18000')->get();
        foreach ($images as $image) {
            echo Sandwich::select('nom')->where('id', $image->s_id)->groupBy('nom')->distinct()->first() . "\n";
        }

    }

//5.7. lister les catégories qui possèdent des images de types 'image/jpeg' de taille > 18000
    public function e57()
    {
        $images = Image::with('sandwich')->where('type', 'image/jpeg')
            ->where('taille', '>', '18000')->get();
        foreach ($images as $image) {
            $sandwich = $image->sandwich;
            foreach ($sandwich->categories as $category) {
                echo "Category: $category->nom \n";
            }
        }

    }
//5.8. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000 et qui
//sont de catégorie 'traditionnel',
    public function e58()
    {
        $images = Image::with('sandwich')->where('type', 'image/jpeg')
            ->where('taille', '>', '18000')->get();
        foreach ($images as $image) {
            $sandwich = $image->sandwich;
            foreach ($sandwich->categories as $category) {
                if ($category->nom == 'traditionnel') {
                    echo $sandwich->nom;
                }
            }
        }
    }

//5.9. pour le sandwich d'ID 7, lister les tailles pour lequel il est disponible avec un prix < 7.0
    public function e59()
    {
        $sandwich = Sandwich::with('tailles')->where('id', 7)->first();
        $tailles = $sandwich->tailles;

        foreach ($tailles as $taille) {
            if ($taille->pivot->prix < 7.00) {
                echo "Taille nom: $taille->nom \n";
            }
        }
    }


}