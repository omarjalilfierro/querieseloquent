<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 11/20/18
 * Time: 9:25 AM
 */

require_once 'vendor/autoload.php';
require_once 'Functions.php';

use catawich\models\Categorie;
use catawich\models\Image;
use catawich\models\Sandwich;
use catawich\models\Taille;
use Illuminate\Database\Eloquent\ModelNotFoundException;

$config = parse_ini_file('conf/config.ini');

$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config );
$db->setAsGlobal();
$db->bootEloquent();
$exec = new Functions();

/*Simple Requests*/
//1.1. lister les sandwichs du catalogue, afficher leur nom, description, type de pain,
//$exec->e11();

//1.2. idem, en triant selon le type_pain,
//$exec->e12();

//1.3. afficher le sandwich n° 42 s'il existe, sinon afficher un message indiquant qu'il n'existe pas.
//Utiliser l'exception ModelNotFoundException.
//$exec->e13();

//1.4. afficher les sandwichs dont le type_pain contient 'baguette', triés par type_pain
//$exec->e14();

//1.5. créer un nouveau sandwich et l'insérer dans la base.
//$exec->e15();

/************************************************Associations 1-n*******************************************************/
//2.1. afficher le sandwich n°4 et lister les images associées,
//$exec->e21();

//2.2. lister l'ensemble des sandwichs, triés par type de pain, et pour chaque sandwich afficher la
//liste des images associées. Utiliser un chargement lié.
//$exec->e22();

//2.3. lister les images et indiquer pour chacune d'elle le sandwich associé en affichant son nom et
//son type de pain.
//$exec->e23();

//2.4. créer 3 images associées au sandwich ajouté dans l'exercice 1.
//$exec->e24();

//2.5. changer le sandwich associé à la 3ème image créée et le remplacer par le sandwich d'Id 6
//$exec->e25();

/************************************************Associations N-N **********************************************************/
//3.1. lister les catégories du sandwich d'ID 5 ; afficher le sandwich (nom, description, type de
//pain) et le nom de chacune des catégories auxquelles il est associé.
//$exec->e31();

//3.2. lister l'ensemble des catégories, et pour chaque catégorie la liste de sandwichs associés ;
//utiliser un chargement lié.
//$exec->e32();

//3.3. lister les sandwichs dont le type_pain contient 'baguette' et pour chaque sandwich, afficher
//ses catégories et la liste des images qui lui sont associées ; utiliser un chargement lié.
//$exec->e33();

//3.4. associer le sandwich créé au 1.5 aux catégories 1 et 3.
//$exec->e34();


/************************************************ Attributs d'associations**********************************************************/

//4.1. afficher la liste des tailles proposées pour le sandwich d'ID 5
//$exec->e41();

//4.2. idem, mais en ajoutant le prix du sandwich pour chaque taille
//$exec->e42();

//4.3. associer le sandwich créé au 1.5 aux différentes tailles existantes en précisant le prix dans
//chaque cas.
//$exec->e43();

/********************************************* Requêtes sur des associations **********************************************************/

//5.1. pour la catégorie dont le nom contient 'traditionnel', lister les sandwichs dont le type_pain
//contient 'baguette',
//$exec->e51();

//5.2. pour le sandwich d'ID 5, lister les images de type 'image/jpeg' et de def_X > 720,
//$exec->e52();

//5.3. lister les sandwichs qui ont plus de 4 images associées,
//$exec->e53();

//5.4. lister les catégories qui ont plus de 6 images associées,
//$exec->e54();

//5.5. lister les catégories qui contiennent des sandwichs dont le type de pain est 'baguette',
//$exec->e55();

//5.6. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000
//$exec->e56();

//5.7. lister les catégories qui possèdent des images de types 'image/jpeg' de taille > 18000
//$exec->e57();

//5.8. lister les sandwichs qui possèdent des images de types 'image/jpeg' de taille > 18000 et qui
//sont de catégorie 'traditionnel',
//$exec->e58();

//5.9. pour le sandwich d'ID 7, lister les tailles pour lequel il est disponible avec un prix < 7.0
//$exec->e59();